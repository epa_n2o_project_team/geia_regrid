#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# description
# ----------------------------------------------------------------------

# A top-level driver for creating an inventory of marine N2O emissions over the AQMEII-NA domain from the GEIA global inventory.
# Converts GEIA marine N2O emissions in native format, spatiotemporality, and units to netCDF over AQMEII in CMAQ-preferred units.

# See https://bitbucket.org/epa_n2o_project_team/geia_regrid

# Expects to run on linux. Utility dependencies include:

# * definitely

# ** bash: required to run this script. Known to work with bash --version==3.2.25

# * potentially initially. If you have not already retrieved regrid_utils (see `get_regrid_utils`), you will need

# ** git: to clone their repo
# ** web access (HTTP currently)

#   These must be available to the script when initially run.

# * ultimately

# ** basename, dirname
# ** cp
# ** curl or wget (latter {preferred, coded} for ability to deal with redirects)
# ** gunzip, unzip
# ** ncdump
# ** NCL
# ** R

#   Paths to the above can be setup in `bash_utilities::setup_paths`

# TODO: failing functions should fail this (entire) driver!

# Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(dirname ${THIS})"

### workspace
# note: following will fail if `source`ing!
export WORK_DIR="${THIS_DIR}" # where we expect to find stuff

### downloading
WGET_TO_FILE='wget --no-check-certificate -c -O'
WGET_TO_STREAM="${WGET_TO_FILE} -"
CURL_TO_FILE='curl -C - -o'
CURL_TO_STREAM='curl'

# model/inventory constants
export MODEL_YEAR='2008'
export MOLAR_MASS_N2O='44.0128' # grams per mole of N2O, per wolframalpha.com
export MOLAR_MASS_N='14.0067'   # molar mass of N2 (per wolframalpha.com)/2
PROJECT_WEBSITE='https://bitbucket.org/epa_n2o_project_team/geia_regrid'
GEIA_ATTR_HISTORY="see ${PROJECT_WEBSITE}"

### AQMEII-NA constants

## datavar attributes
# TODO: get from template file or from EPIC
# "real" data
AQMEIINA_DV_NAME='N2O'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_DV_LONG_NAME="$(printf '%-16s' ${AQMEIINA_DV_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_DV_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
# Don't single-quote the payload: double-quote it (OK inside parens inside double-quotes)
AQMEIINA_DV_VAR_DESC="$(printf '%-80s' "Model species ${AQMEIINA_DV_NAME}")"

## "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
AQMEIINA_TFLAG_NAME='TFLAG'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_TFLAG_LONG_NAME="$(printf '%-16s' ${AQMEIINA_TFLAG_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_TFLAG_UNITS="$(printf '%-16s' '<YYYYDDD,HHMMSS>')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
AQMEIINA_TFLAG_VAR_DESC="$(printf '%-80s' 'Timestep-valid flags:  (1) YYYYDDD or (2) HHMMSS')"

## dimensions and their attributes
AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_LAYER_LONG_NAME='index of layers above surface'
AQMEIINA_DIM_LAYER_UNITS='unitless'

AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_TSTEP_UNITS='' # they vary!
AQMEIINA_DIM_TSTEP_LONG_NAME='timestep'

# dimensions we don't really need, but VERDI/IOAPI/TFLAG does
AQMEIINA_DIM_DATETIME_NAME='DATE-TIME'
AQMEIINA_DIM_VAR_NAME='VAR'

AQMEIINA_DIM_X_N='459'
AQMEIINA_DIM_X_NAME='COL'
# AQMEIINA_DIM_X_UNITS='unitless' # my invention
AQMEIINA_DIM_X_UNITS='m'
# AQMEIINA_DIM_X_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_X_LONG_NAME='grid-center offset from center of projection'

AQMEIINA_DIM_Y_N='299'
AQMEIINA_DIM_Y_NAME='ROW'
# AQMEIINA_DIM_Y_UNITS='unitless' # my invention
AQMEIINA_DIM_Y_UNITS='m'
# AQMEIINA_DIM_Y_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_Y_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"

### for visualization (generally)
export OUTPUT_SIGNIFICANT_DIGITS='3' # see conservation report below

## for plotting (specifically)
# PDF_VIEWER='xpdf' # set this in bash_utilities::setup_apps
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${WORK_DIR}"

# ----------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------

### helpers in this repo

export CALL_REFORMAT_FN='reformat_GEIA_to_netCDF.r'
export CALL_REFORMAT_FP="${WORK_DIR}/${CALL_REFORMAT_FN}"
export CALL_REGRID_FN='regrid_global_to_AQMEII.r'
export CALL_REGRID_FP="${WORK_DIR}/${CALL_REGRID_FN}"
export CALL_RETEMP_FN='retemp_reunit.ncl'
export CALL_RETEMP_FP="${WORK_DIR}/${CALL_RETEMP_FN}"
export WRITE_IOAPI_FN='write_IOAPI.ncl'
export WRITE_IOAPI_FP="${WORK_DIR}/${WRITE_IOAPI_FN}"
export CALL_CONSERV_FN='check_conservation.ncl'
export CALL_CONSERV_FP="${WORK_DIR}/${CALL_CONSERV_FN}"

### helpers retrieved from elsewhere. TODO: create NCL and R packages

# path to a project, not a .git
REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# can also use   'git@bitbucket.org:tlroche/regrid_utils' if supported
# path to a folder, not a file: needed by NCL to get to initial helpers
export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

export BASH_UTILS_FN='bash_utilities.sh'
# export BASH_UTILS_FP="${REGRID_UTILS_DIR}/${BASH_UTILS_FN}"
# no, allow user to override repo code: see `get_bash_utils`
export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

export IOAPI_FUNCS_FN='IOAPI.ncl'
export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

export PLOT_FUNCS_FN='plotLayersForTimestep.r'
export PLOT_FUNCS_FP="${WORK_DIR}/${PLOT_FUNCS_FN}"

export STATS_FUNCS_FN='netCDF.stats.to.stdout.r'
export STATS_FUNCS_FP="${WORK_DIR}/${STATS_FUNCS_FN}"

export STRING_FUNCS_FN='string.ncl'
export STRING_FUNCS_FP="${WORK_DIR}/${STRING_FUNCS_FN}"

export SUMMARIZE_FUNCS_FN='summarize.ncl'
export SUMMARIZE_FUNCS_FP="${WORK_DIR}/${SUMMARIZE_FUNCS_FN}"

export TIME_FUNCS_FN='time.ncl'
export TIME_FUNCS_FP="${WORK_DIR}/${TIME_FUNCS_FN}"

export VIS_FUNCS_FN='visualization.r'
export VIS_FUNCS_FP="${WORK_DIR}/${VIS_FUNCS_FN}"

# ----------------------------------------------------------------------
# inputs
# ----------------------------------------------------------------------

# Raw input data is GEIA marine N2O emissions in native format.
# For details, see http://geiacenter.org/presentData/geiadfrm.html
# Get from my repository (stable location, no unzip)
export GEIA_RAW_SOURCE_FILE="http://geiacenter.org/data/n2ooc90y.1a.zip, 'GEIA Inventory n2ocg90yr1.1a  18 Dec 95'"
export GEIA_RAW_CONVENTIONS='Contact: A.F. Bouwman, RIVM; Box 1, 3720 BA Bilthoven, Netherlands; tel.31-30-2743635; fax.31-30-2744417; email lex.bouwman@rivm.nl'
GEIA_RAW_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/N2OOC90Y.1A'
GEIA_RAW_FN="$(basename ${GEIA_RAW_URI})"
export GEIA_RAW_FP="${WORK_DIR}/${GEIA_RAW_FN}"

export GEIA_RAW_HEADER_N='10'       # header.n lines to skip @ top
# GEIA data is 1° x 1°, so ...
export GEIA_RAW_LAT_DEGREE_START='-90.0'   # 90 S, scalar
export GEIA_RAW_LAT_DEGREE_PER_CELL='1.0'
export GEIA_RAW_LON_DEGREE_START='-180.0'  # 180 W, scalar
export GEIA_RAW_LON_DEGREE_PER_CELL='1.0'

GEIA_RAW_PDF_FN="geia_raw_$(date +${DATE_FORMAT}).pdf"
export GEIA_RAW_PDF_FP="${WORK_DIR}/${GEIA_RAW_PDF_FN}" # path to PDF output

### Template for both `raster` extents and IOAPI-writing

# This file should have all required IOAPI metadata (including the "fake datavar"=TFLAG) for a single datavar=N2O.
# We'll use it to output IOAPI, so we can check grid alignment in VERDI.
# It must also have the correct AQMEII-NA extents, to use in `raster` regridding.
TEMPLATE_IOAPI_GZ_URI='https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil/downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz' # final yearly output from AQMEII_ag_soil, VERDI-viewable
# formerly also used
# TEMPLATE_EXTENT_GZ_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/emis_mole_all_20080101_12US1_cmaq_cb05_soa_2008ab_08c.EXTENTS_INPUT.nc.gz'
# et al
TEMPLATE_IOAPI_GZ_FN="$(basename ${TEMPLATE_IOAPI_GZ_URI})"
TEMPLATE_IOAPI_GZ_FP="${WORK_DIR}/${TEMPLATE_IOAPI_GZ_FN}"
# TEMPLATE_IOAPI_ROOT="${TEMPLATE_IOAPI_GZ_FN%.*}" # everything left of the LAST dot
# TEMPLATE_IOAPI_FN="${TEMPLATE_IOAPI_ROOT}"
# that will collide with this project's final output! so instead
TEMPLATE_IOAPI_FN='aqmeii_ag_soil_yearly.nc'
export TEMPLATE_IOAPI_FP="${WORK_DIR}/${TEMPLATE_IOAPI_FN}"
export TEMPLATE_IOAPI_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export TEMPLATE_IOAPI_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
export TEMPLATE_IOAPI_BAND='1' # since position of TSTEP in N2O(TSTEP, LAY, ROW, COL)? seems to work

# ----------------------------------------------------------------------
# intermediate products
# ----------------------------------------------------------------------

# Raw input reformated to netCDF

GEIA_REFORMAT_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/GEIA_N2O_oceanic.nc'
GEIA_REFORMAT_FN="$(basename ${GEIA_REFORMAT_URI})"
export GEIA_REFORMAT_FP="${WORK_DIR}/${GEIA_REFORMAT_FN}"

# TODO: automate getting this metadata
export GEIA_REFORMAT_DATAVAR_NAME='emi_n2o'            # like EDGAR
export GEIA_REFORMAT_DATAVAR_LONGNAME='N2O emissions'  # like CLM-CN
export GEIA_REFORMAT_DATAVAR_UNIT='ton N2O-N/yr'       # value from header(GEIA.emis.txt.fn)
export GEIA_REFORMAT_DATAVAR_NA='-999.0'               # like GFED
export GEIA_REFORMAT_DATAVAR_BAND='3' # index of dim=TIME in emi_n2o(time, lat, lon)
export GEIA_REFORMAT_DATAVAR_TOTAL='3.5959E+06'        # value from header(GEIA.emis.txt.fn)
export GEIA_REFORMAT_TIME_VAR_NAME='time'              # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_TIME_VAR_LONG_NAME='time'         # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_TIME_VAR_UNITS='year'             # for now
export GEIA_REFORMAT_TIME_VAR_CALENDAR='none: emissions attributed to no particular year'
export GEIA_REFORMAT_LAT_VAR_NAME='lat'                # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_LAT_VAR_LONG_NAME='latitude'      # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_LAT_VAR_UNITS='degrees_north'     # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_LAT_VAR_COMMENT='center_of_cell'  # like EDGAR
export GEIA_REFORMAT_LON_VAR_NAME='lon'                # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_LON_VAR_LONG_NAME='longitude'     # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_LON_VAR_UNITS='degrees_east'      # like CLM-CN, EDGAR, GFED
export GEIA_REFORMAT_LON_VAR_COMMENT='center_of_cell'  # like EDGAR

GEIA_REFORMAT_PDF_FN="geia_reformat_$(date +${DATE_FORMAT}).pdf"
export GEIA_REFORMAT_PDF_FP="${WORK_DIR}/${GEIA_REFORMAT_PDF_FN}"
export GEIA_REFORMAT_PDF_TITLE='GEIA annual oceanic N2O emissions'

# ----------------------------------------------------------------------
# final output: regridded to AQMEII-NA
# ----------------------------------------------------------------------

GEIA_REGRID_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/GEIA_N2O_oceanic_regrid.nc'
GEIA_REGRID_FN="$(basename ${GEIA_REGRID_URI})"
export GEIA_REGRID_FP="${WORK_DIR}/${GEIA_REGRID_FN}"
# TODO: automate getting this metadata
export GEIA_REGRID_DATAVAR_NAME="${GEIA_REFORMAT_DATAVAR_NAME}"
export GEIA_REGRID_X_VAR_NAME="${AQMEIINA_DIM_X_NAME}"
export GEIA_REGRID_Y_VAR_NAME="${AQMEIINA_DIM_Y_NAME}"
export GEIA_REGRID_DATAVAR_UNIT="${GEIA_REFORMAT_DATAVAR_UNIT}"
# export GEIA_REGRID_PDF_TITLE="GEIA annual oceanic N2O emissions regridded to AQMEII-NA\n${GEIA_REGRID_DATAVAR_UNIT}"
export GEIA_REGRID_PDF_TITLE='GEIA annual oceanic N2O emissions regridded to AQMEII-NA'

# ----------------------------------------------------------------------
# retemporalization and re-unit-ing
# ----------------------------------------------------------------------

## create data container files from extents/template file
# problem: NCL currently (version=6.1.2) does not support *.ncf :-( workaround below
# replace a string with values @ runtime
# export GEIA_TARGET_FN_TEMPLATE_STR='######' # works for NCL, not bash
export GEIA_TARGET_FN_TEMPLATE_STR='@@@@@@'
GEIA_TARGET_FN_ROOT_TEMPLATE="emis_mole_N2O_${GEIA_TARGET_FN_TEMPLATE_STR}_12US1_cmaq_cb05_soa_2008ab_08c"
GEIA_TARGET_EXT_CMAQ='ncf'
GEIA_TARGET_EXT_NCL='nc'
GEIA_TARGET_FN_TEMPLATE_CMAQ="${GEIA_TARGET_FN_ROOT_TEMPLATE}.${GEIA_TARGET_EXT_CMAQ}"
GEIA_TARGET_FN_TEMPLATE_NCL="${GEIA_TARGET_FN_ROOT_TEMPLATE}.${GEIA_TARGET_EXT_NCL}"
export GEIA_TARGET_FP_TEMPLATE_CMAQ="${WORK_DIR}/${GEIA_TARGET_FN_TEMPLATE_CMAQ}"
export GEIA_TARGET_FP_TEMPLATE_NCL="${WORK_DIR}/${GEIA_TARGET_FN_TEMPLATE_NCL}"
export GEIA_TARGET_FP_CMAQ="${GEIA_TARGET_FP_TEMPLATE_CMAQ/${GEIA_TARGET_FN_TEMPLATE_STR}/${MODEL_YEAR}}"
export GEIA_TARGET_FP_NCL="${GEIA_TARGET_FP_TEMPLATE_NCL/${GEIA_TARGET_FN_TEMPLATE_STR}/${MODEL_YEAR}}"

## global metadata for both {template, "real" monthly/hourly data container} files
export GEIA_TARGET_FILE_ATTR_FILEDESC='annual marine N2O emissions over AQMEII-NA in mol/s, IOAPI-zed'
export GEIA_TARGET_FILE_ATTR_HISTORY="${GEIA_ATTR_HISTORY}"

## metadata for datavar in both {template, "real" monthly/hourly data container} files
export GEIA_TARGET_DATAVAR_NAME='N2O'
# IOAPI pads varattr=long_name to length=16 with spaces
export GEIA_TARGET_DATAVAR_ATTR_LONG_NAME="$(printf '%-16s' ${GEIA_TARGET_DATAVAR_NAME})"
# IOAPI pads varattr=units to length=16 with spaces
export GEIA_TARGET_DATAVAR_ATTR_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with spaces
# export GEIA_TARGET_DATAVAR_ATTR_VAR_DESC="$(printf '%-80s' 'Model species ${GEIA_TARGET_DATAVAR_NAME}')"
# export GEIA_TARGET_DATAVAR_ATTR_VAR_DESC="$(printf '%-80s' \"Model species ${GEIA_TARGET_DATAVAR_NAME}\")"
export GEIA_TARGET_DATAVAR_ATTR_VAR_DESC="$(printf '%-80s' "Model species ${GEIA_TARGET_DATAVAR_NAME}")"

## conservation report constants
# need 5 additional digits for float output, e.g., "4.85e+04"
# bash arithmetic gotcha: allow no spaces around '='!
export OUTPUT_SIGNIFICANT_DIGITS='3' # see conservation report below
# export CONSERV_REPORT_FIELD_WIDTH=$((OUTPUT_SIGNIFICANT_DIGITS + 5))
export CONSERV_REPORT_FIELD_WIDTH='9' # width of 'AQMEII-NA'
export CONSERV_REPORT_FLOAT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}.$((OUTPUT_SIGNIFICANT_DIGITS - 1))e"
# echo -e "${THIS_FN}: CONSERV_REPORT_FLOAT_FORMAT=${CONSERV_REPORT_FLOAT_FORMAT}" # debugging
export CONSERV_REPORT_INT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}i"
export CONSERV_REPORT_COLUMN_SEPARATOR="  "
export CONSERV_REPORT_TITLE="Is N2O conserved from input to output? units=ton-N"
# TODO: learn ocean area in AQMEII-NA vs world ocean area (à la CLM-CN checker)
export CONSERV_REPORT_SUBTITLE='(TODO: compare global ocean area to AQMEII-NA ocean area)'

# map*.dat is used to create a map for image.plot-ing
# use one of following, depending on units

# units=km
# MAP_TABLE_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/map.CMAQkm.world.dat'
# units=m
MAP_TABLE_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/map.CMAQm.world.dat'
MAP_TABLE_FN="$(basename ${MAP_TABLE_URI})"
export MAP_TABLE_FP="${WORK_DIR}/${MAP_TABLE_FN}"

GEIA_REGRID_PDF_FN="geia_regrid_$(date +${DATE_FORMAT}).pdf"
export GEIA_REGRID_PDF_FP="${WORK_DIR}/${GEIA_REGRID_PDF_FN}"

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup functions
# ----------------------------------------------------------------------

# TODO: test for resources, reuse if available
# {setup_paths, setup_apps} isa bash util, so `get_helpers` first
function setup {
  for CMD in \
    "mkdir -p ${WORK_DIR}" \
    'get_helpers' \
    'setup_paths' \
    'setup_apps' \
    'setup_resources' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${CMD}' not defined"
      exit 1
    else
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    fi
  done
#  echo -e "\n$ ${THIS_FN}: PDF_VIEWER='${PDF_VIEWER}'" # debugging
} # end function setup

# Must `get_regrid_utils` first.
function get_helpers {
  for CMD in \
    'get_regrid_utils' \
    'get_bash_utils' \
    'get_IOAPI_funcs' \
    'get_plot_funcs' \
    'get_stats_funcs' \
    'get_string_funcs' \
    'get_summarize_funcs' \
    'get_time_funcs' \
    'get_reformat' \
    'get_regrid' \
    'get_retemp' \
    'get_conserv' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${CMD}' not defined"
      exit 2
    else
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    fi
  done
} # end function get_helpers

# This gets "package"=regrid_utils from which the remaining helpers are (at least potentially) drawn.
function get_regrid_utils {
  if [[ -z "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: REGRID_UTILS_DIR not defined"
    exit 3
  fi

  # Noticed a problem where code committed to regrid_utils was not replacing code in existing ${REGRID_UTILS_DIR}, so ...
  if [[ -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: removing existing regrid_utils folder='${REGRID_UTILS_DIR}'"
    for CMD in \
      "rm -fr ${REGRID_UTILS_DIR}/" \
     ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 4
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
  # assumes GIT_CLONE_PREFIX set in environment by, e.g., uber_driver
    for CMD in \
      "${GIT_CLONE_PREFIX} git clone ${REGRID_UTILS_URI}.git" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo # newline
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found"
        echo -e "\t(suggestion: check that GIT_CLONE_PREFIX is set appropriately in calling environment)"
        echo # newline
        exit 5
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download regrid_utils to '${REGRID_UTILS_DIR}'"
    exit 6
  fi  
} # end function get_regrid_utils

#    'get_template_extent' \
function setup_resources {
  for CMD in \
    'get_map_table' \
    'get_geia_raw' \
    'get_template_IOAPI' \
    'put_geia_target_ncl' \
    'put_geia_target_cmaq' \
    'get_geia_reformat' \
    'get_geia_regrid' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${CMD}' not defined"
      exit 3
    else
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    fi
  done
} # end function setup_resources

function get_map_table {
  if [[ -z "${MAP_TABLE_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: MAP_TABLE_FP not defined"
    exit 4
  fi
  if [[ ! -r "${MAP_TABLE_FP}" ]] ; then
    for CMD in \
      "wget --no-check-certificate -c -O ${MAP_TABLE_FP} ${MAP_TABLE_URI}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${MAP_TABLE_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download MAP_TABLE_FP=='${MAP_TABLE_FP}'"
    exit 5
  fi
} # end function get_map_table

function get_geia_raw {
  if [[ -z "${GEIA_RAW_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GEIA_RAW_FP not defined"
    exit 6
  fi
  if [[ ! -r "${GEIA_RAW_FP}" ]] ; then
    for CMD in \
      "wget --no-check-certificate -c -O ${GEIA_RAW_FP} ${GEIA_RAW_URI}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${GEIA_RAW_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download GEIA_RAW_FP=='${GEIA_RAW_FP}'"
    exit 7
  fi
} # end function get_geia_raw

### Get "template" file used for regridding, IOAPI-writing.
function get_template_IOAPI {
  if [[ -z "${TEMPLATE_IOAPI_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TEMPLATE_IOAPI_FP not defined"
    exit 8
  fi
  if [[ ! -r "${TEMPLATE_IOAPI_FP}" ]] ; then
    if [[ ! -r "${TEMPLATE_IOAPI_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${TEMPLATE_IOAPI_GZ_FP} ${TEMPLATE_IOAPI_GZ_URI}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
    if [[ -r "${TEMPLATE_IOAPI_GZ_FP}" ]] ; then
      # JIC ${TEMPLATE_IOAPI_FP} != ${TEMPLATE_IOAPI_GZ_FP} - .gz
      for CMD in \
        "gunzip -c ${TEMPLATE_IOAPI_GZ_FP} > ${TEMPLATE_IOAPI_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
  fi
  if [[ ! -r "${TEMPLATE_IOAPI_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read TEMPLATE_IOAPI_FP=='${TEMPLATE_IOAPI_FP}'"
    exit 9
  fi
} # end function get_template_IOAPI

function put_geia_target_ncl {
  if [[ -z "${GEIA_TARGET_FP_NCL}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GEIA_TARGET_FP_NCL not defined"
    exit 10
  fi
  if [[ -r "${GEIA_TARGET_FP_NCL}" ]] ; then
    # delete: NCL will recreate
    for CMD in \
      "rm ${GEIA_TARGET_FP_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${GEIA_TARGET_FP_NCL}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot delete GEIA_TARGET_FP_NCL=='${GEIA_TARGET_FP_NCL}'"
    exit 11
  fi
} # end function put_geia_target_ncl

function put_geia_target_cmaq {
  if [[ -z "${GEIA_TARGET_FP_CMAQ}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GEIA_TARGET_FP_CMAQ not defined"
    exit 12
  fi
  if [[ -r "${GEIA_TARGET_FP_CMAQ}" ]] ; then
    # delete: NCL will recreate
    for CMD in \
      "rm ${GEIA_TARGET_FP_CMAQ}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${GEIA_TARGET_FP_CMAQ}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot delete GEIA_TARGET_FP_CMAQ=='${GEIA_TARGET_FP_CMAQ}'"
    exit 13
  fi
} # end function put_geia_target_cmaq

function get_geia_reformat {
  if [[ -z "${GEIA_REFORMAT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GEIA_REFORMAT_FP not defined"
    exit 14
  fi
# only for testing. TODO: flag control
#   if [[ ! -r "${GEIA_REFORMAT_FP}" ]] ; then
#     for CMD in \
#       "wget --no-check-certificate -c -O ${GEIA_REFORMAT_FP} ${GEIA_REFORMAT_URI}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
#   if [[ ! -r "${GEIA_REFORMAT_FP}" ]] ; then
#     echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download GEIA_REFORMAT_FP=='${GEIA_REFORMAT_FP}'"
#     exit 15
#   fi
} # end function get_geia_reformat

function get_geia_regrid {
  if [[ -z "${GEIA_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GEIA_REGRID_FP not defined"
    exit 16
  fi
# only for testing. TODO: flag control
#   if [[ ! -r "${GEIA_REGRID_FP}" ]] ; then
#     for CMD in \
#       "wget --no-check-certificate -c -O ${GEIA_REGRID_FP} ${GEIA_REGRID_URI}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
#   if [[ ! -r "${GEIA_REGRID_FP}" ]] ; then
#     echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download GEIA_REGRID_FP=='${GEIA_REGRID_FP}'"
#     exit 17
#   fi
} # end function get_geia_regrid

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${BASH_UTILS_FP} before running this script.
function get_bash_utils {
  if [[ -z "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP not defined"
    exit 18
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${BASH_UTILS_FN} ${BASH_UTILS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP=='${BASH_UTILS_FP}' not readable"
    exit 19
  fi
  # This is bash, so gotta ...
  source "${BASH_UTILS_FP}"
  # ... for its functions to be available later in this script
} # end function get_bash_utils

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${IOAPI_FUNCS_FP} before running this script.
function get_IOAPI_funcs {
  if [[ -z "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: IOAPI_FUNCS_FP not defined"
    exit 24
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${IOAPI_FUNCS_FN} ${IOAPI_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: IOAPI_FUNCS_FP=='${IOAPI_FUNCS_FP}' not readable"
    exit 25
  fi
} # end function get_IOAPI_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${PLOT_FUNCS_FP} before running this script.
function get_plot_funcs {
  if [[ -z "${PLOT_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: PLOT_FUNCS_FP not defined"
    exit 20
  fi
  if [[ ! -r "${PLOT_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${PLOT_FUNCS_FN} ${PLOT_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${PLOT_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: PLOT_FUNCS_FP=='${PLOT_FUNCS_FP}' not readable"
    exit 21
  fi
} # end function get_plot_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STATS_FUNCS_FP} before running this script.
function get_stats_funcs {
  if [[ -z "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP not defined"
    exit 22
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STATS_FUNCS_FN} ${STATS_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP=='${STATS_FUNCS_FP}' not readable"
    exit 23
  fi
} # end function get_stats_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STRING_FUNCS_FP} before running this script.
function get_string_funcs {
  if [[ -z "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP not defined"
    exit 24
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STRING_FUNCS_FN} ${STRING_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP=='${STRING_FUNCS_FP}' not readable"
    exit 25
  fi
} # end function get_string_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${SUMMARIZE_FUNCS_FP} before running this script.
function get_summarize_funcs {
  if [[ -z "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP not defined"
    exit 26
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${SUMMARIZE_FUNCS_FN} ${SUMMARIZE_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP=='${SUMMARIZE_FUNCS_FP}' not readable"
    exit 27
  fi
} # end function get_summarize_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${TIME_FUNCS_FP} before running this script.
function get_time_funcs {
  if [[ -z "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP not defined"
    exit 28
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${TIME_FUNCS_FN} ${TIME_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP=='${TIME_FUNCS_FP}' not readable"
    exit 29
  fi
} # end function get_time_funcs

function get_reformat {
  if [[ -z "${CALL_REFORMAT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_REFORMAT_FP not defined"
    exit 30
  fi
  # is in this repo
#  if [[ ! -r "${CALL_REFORMAT_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_REFORMAT_FP} ${REFORMAT_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_REFORMAT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_REFORMAT_FP=='${CALL_REFORMAT_FP}' not readable"
    exit 31
  fi
} # end function get_reformat

function get_regrid {
  if [[ -z "${CALL_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_REGRID_FP not defined"
    exit 32
  fi
  # is in this repo
#  if [[ ! -r "${CALL_REGRID_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_REGRID_FP} ${REGRID_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_REGRID_FP=='${CALL_REGRID_FP}' not readable"
    exit 33
  fi
} # end function get_regrid

function get_retemp {
  if [[ -z "${CALL_RETEMP_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_RETEMP_FP not defined"
    exit 34
  fi
  # is in this repo
#  if [[ ! -r "${CALL_RETEMP_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_RETEMP_FP} ${RETEMP_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_RETEMP_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_RETEMP_FP=='${CALL_RETEMP_FP}' not readable"
    exit 35
  fi
} # end function get_retemp

function get_conserv {
  if [[ -z "${CALL_CONSERV_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_CONSERV_FP not defined"
    exit 36
  fi
  # is in this repo
#  if [[ ! -r "${CALL_CONSERV_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_CONSERV_FP} ${CONSERV_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_CONSERV_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_CONSERV_FP=='${CALL_CONSERV_FP}' not readable"
    exit 37
  fi
} # end function get_conserv

# ----------------------------------------------------------------------
# action functions
# ----------------------------------------------------------------------

### reformat from native to netCDF
function reformat {
  # If this fails ...
  for CMD in \
    "Rscript ${CALL_REFORMAT_FP}" \
  ; do
  cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
    eval "${CMD}"
  done
  # ... just start R ...
#  R
  # ... and source ${CALL_REFORMAT_FP}, e.g.,
#   source('....r')

  # After exiting R, show cwd and display output PDF.
  for CMD in \
    "ls -alht ${WORK_DIR}" \
    "${PDF_VIEWER} ${GEIA_REFORMAT_PDF_FP} &" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
} # end function reformat

### regrid from global/lon-lat to AQMEII-NA/LCC
function regrid {
  # If this fails ...
  for CMD in \
    "Rscript ${CALL_REGRID_FP}" \
  ; do
  cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
    eval "${CMD}"
  done
  # ... just start R ...
#  R
  # ... and source ${CALL_REGRID_FP}, e.g.,
#   source('....r')

  # After exiting R, show cwd and display output PDF.
  for CMD in \
    "ls -alht ${WORK_DIR}" \
    "${PDF_VIEWER} ${GEIA_REGRID_PDF_FP} &" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
} # end function regrid

### "retemporalize" from annual to hourly (and put in IOAPI-zed CMAQ-style file)
function retemp_IOAPI {
  # remove any pre-existing output data container files

#   # start debugging-----------------------------------------------------
#   echo -e "${THIS_FN}: GEIA_TARGET_FP_TEMPLATE_CMAQ='${GEIA_TARGET_FP_TEMPLATE_CMAQ}'"
#   echo -e "${THIS_FN}: GEIA_TARGET_FP_TEMPLATE_NCL= '${GEIA_TARGET_FP_TEMPLATE_NCL}'"
#   echo -e "${THIS_FN}: GEIA_TARGET_FP_CMAQ=         '${GEIA_TARGET_FP_CMAQ}'"
#   echo -e "${THIS_FN}: GEIA_TARGET_FP_NCL=          '${GEIA_TARGET_FP_NCL}'"
#   #   end debugging-----------------------------------------------------

  if [[ -r "${GEIA_TARGET_FP_CMAQ}" ]] ; then
    for CMD in \
      "rm ${GEIA_TARGET_FP_CMAQ}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

  if [[ -r "${GEIA_TARGET_FP_TEMPLATE_CMAQ}" ]] ; then
    for CMD in \
      "rm ${GEIA_TARGET_FP_TEMPLATE_CMAQ}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

  if [[ -r "${GEIA_TARGET_FP_NCL}" ]] ; then
    for CMD in \
      "rm ${GEIA_TARGET_FP_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

  if [[ -r "${GEIA_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    for CMD in \
      "rm ${GEIA_TARGET_FP_TEMPLATE_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

#   # start debugging-----------------------------------------------------
#   for CMD in \
#     "ls -alh ${GEIA_TARGET_FP_NCL}" \
#     "ls -alh ${GEIA_TARGET_FP_CMAQ}" \
#   ; do
#     echo -e "$ ${CMD}"
#     eval "${CMD}"
#   done
#   #   end debugging-----------------------------------------------------

#   ncl # bail to NCL and copy script lines, or ...
  # TODO: pass commandline args to NCL
  # punt: just use envvars :-(
  # '-n' -> http://www.ncl.ucar.edu/Document/Functions/Built-in/print.shtml
  for CMD in \
    "ncl -n ${CALL_RETEMP_FP}" \
  ; do
  cat <<EOM

About to run command="${CMD}"

EOM
    eval "${CMD}"
  done

  # don't move, copy: NCL won't *read* .ncf either :-(
#     "mv ${GEIA_TARGET_FP_NCL} ${GEIA_TARGET_FP_CMAQ}" \
  for CMD in \
    "cp ${GEIA_TARGET_FP_NCL} ${GEIA_TARGET_FP_CMAQ}" \
    "ls -alht ${WORK_DIR}/*.${GEIA_TARGET_EXT_CMAQ}" \
    "ls -alht ${WORK_DIR}/*.${GEIA_TARGET_EXT_NCL}" \
  ; do
# done in `teardown`
#     "ncdump -h ${GEIA_TARGET_FP_CMAQ}" \
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
} # end function retemp_IOAPI

### Write IOAPI-compatible output, view with VERDI
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function write_IOAPI {
#   ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${WRITE_IOAPI_FP}" \
  ; do
  cat <<EOM

About to run command="${CMD}"

EOM
    eval "${CMD}"
  done
} # end function write_IOAPI

### check conservation of mass input -> output
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function check_conserv {
  # ncl bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${CALL_CONSERV_FP}" \
  ; do
  cat <<EOM

About to run command="${CMD}"

EOM
    eval "${CMD}"
  done
} # end function check_conserv

### cleanup/teardown
function teardown {
  ### remove the *.nc required only for NCL

#   # start debugging-----------------------------------------------------
#   echo -e "${THIS_FN}: GEIA_TARGET_FP_TEMPLATE_NCL= '${GEIA_TARGET_FP_TEMPLATE_NCL}'"
#   echo -e "${THIS_FN}: GEIA_TARGET_FP_NCL=          '${GEIA_TARGET_FP_NCL}'"
#   #   end debugging-----------------------------------------------------

  if [[ -r "${GEIA_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    for CMD in \
      "rm ${GEIA_TARGET_FP_TEMPLATE_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${GEIA_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot delete GEIA_TARGET_FP_TEMPLATE_NCL=='${GEIA_TARGET_FP_TEMPLATE_NCL}'"
    exit 38
  fi

  if [[ -r "${GEIA_TARGET_FP_NCL}" ]] ; then
    for CMD in \
      "rm ${GEIA_TARGET_FP_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${GEIA_TARGET_FP_NCL}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot delete GEIA_TARGET_FP_NCL=='${GEIA_TARGET_FP_NCL}'"
    exit 39
  fi

  ### show new plots and annual hourly files
  for CMD in \
    "ls -alt ${WORK_DIR}/*.${GEIA_TARGET_EXT_CMAQ}" \
    "ls -alt ${WORK_DIR}/*.${GEIA_TARGET_EXT_NCL}" \
    "ncdump -h ${GEIA_TARGET_FP_CMAQ}" \
    "ls -alt ${WORK_DIR}/*.pdf" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
} # end function teardown

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

# should always
# * begin with `setup` to setup paths, apps, helpers, resources
# * end with `teardown` for tidy and testing (e.g., plot display)
  # 'setup' \
  # 'reformat' \
  # 'regrid' \
  # 'retemp_IOAPI' \
  # 'check_conserv' \
  # 'teardown' \
for CMD in \
  'setup' \
  'reformat' \
  'regrid' \
  'retemp_IOAPI' \
  'check_conserv' \
  'teardown' \
; do
  if [[ -z "${CMD}" ]] ; then
    echo -e "${THIS_FN}::main loop: ERROR: '${CMD}' not defined"
    exit 40
  else
    echo -e "\n$ ${THIS_FN}::main loop: ${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  fi
done

# ----------------------------------------------------------------------
# debugging
# ----------------------------------------------------------------------
