*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] and [R][R @ wikipedia] code to

1. convert [GEIA][] geospatial data from its quirky [native format][GEIA native format] to [netCDF][].
1. 2D-regrid netCDF data from global/unprojected to a projected subdomain ([AQMEII-NA][]).
1. "reunit" the data from GEIA's `tonN/yr` to `moles/s` (what CMAQ wants).
1. "retemporalize" from yearly (single) timestep to CMAQ-style emissions files (e.g., [this][emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz], when `gunzip`ed) containing hourly emissions usable for any day in the model year=2008. 
1. check that mass is (more or less) conserved in all of the above

Currently does not provide a clean or general-purpose (much less packaged) solution! This project merely shows how to perform these tasks using

* bash (tested with version=3.2.25)
* NCL (tested with version=6.1.2)
* R (tested with version=3.0.0) and packages including
    * [ncdf4][]
    * [fields][]
    * [raster][]

(Note this code was formerly [housed at github][github repo], but was moved due to the [github's change in upload/download policy][github download policy]. Also, IMHO, the bitbucket wikis render more attractively--YMMV.)

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[netCDF]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[GEIA]: http://www.geiacenter.org/
[GEIA native format]: /TomRoche/GEIA_to_netCDF/blob/master/GEIA_readme.txt
[AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz]: ../../downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz
[ncdf4]: http://cran.r-project.org/web/packages/ncdf4/
[fields]: http://cran.r-project.org/web/packages/fields/
[raster]: http://cran.r-project.org/web/packages/raster/
[github repo]: https://github.com/TomRoche/GEIA_to_netCDF/
[github download policy]: https://github.com/blog/1302-goodbye-uploads

# operation

To run this code,

1. `git clone` this repo. (See commandlines on the project homepage, where you probably are now.)
1. `cd` to its working directory (where you cloned it to).
1. Setup your applications and paths.
    1. Download a copy of [these bash utilities][bash_utilities.sh] to the working directory.
    1. Open the file in in an editor! You will probably need to edit its functions `setup_paths` and `setup_apps` to make it work on your platform. Notably you will want to point it to your PDF viewer and NCL and R executables.
    1. You may also want to open the [driver (bash) script][GEIA_driver.sh] in an editor and take a look. It should run Out Of The Box, but you might need to tweak something there. In the worst case, you could hardcode your paths and apps in the driver.
    1. Once you've got it working, you may want to fork it. If so, you can automate running your changes with [uber_driver.sh][] (changing that as needed, too).
1. Run the driver:
        `$ ./GEIA_driver.sh`
    This will download input (and possibly `git clone` the [`regrid_utils`][regrid_utils repository]), then run
    * an [R script][reformat_GEIA_to_netCDF.r] to convert the input to a [netCDF][] file, and plot it. The driver should display [a PDF][reformatting plot] if properly configured.
    * another [R script][regrid_global_to_AQMEII.r] to regrid the netCDF, and plot the output. The driver should display [a PDF][regridding plot] if properly configured. This PDF currently has 2 pages (p1 using `raster::plot` and p2 using `fields::image.plot`).
    * an [NCL script][retemp_reunit.ncl] to convert regridded concentrations to units appropriate for [CMAQ][CMAQ @ CMAS] (and with [IOAPI][] metadata) and display the output using [VERDI][].
    * an [NCL script][check_conservation.ncl] to check conservation of mass from input to output. Given that the output domain (AQMEII-NA) is significantly smaller than the input domain (global), it merely reports the fraction of mass in output vs input, and compares that to an estimation of the land area of the output domain relative to the input domain. Current output is

                Is N2O conserved from input to output? units=ton-N
                (TODO: compare global ocean area to AQMEII-NA ocean area)
                    input      input     output     output           
                   global        NAs  AQMEII-NA        NAs     out/in
                 3.60e+06      28657   1.06e+06      90768   2.96e-01

[bash_utilities.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/bash_utilities.sh?at=master
[GEIA_driver.sh]: ../../src/HEAD/GEIA_driver.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[regrid_utils repository]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[reformatting plot]: ../../downloads/GEIA_N2O_oceanic.pdf
[regridding plot]: ../../downloads/GEIA_N2O_oceanic_regrid.pdf
[GEIA input processing @ project wiki]: https://github.com/TomRoche/cornbeltN2O/wiki/Simulation-of-N2O-Production-and-Transport-in-the-US-Cornbelt-Compared-to-Tower-Measurements#wiki-input-processing-GEIA
[CMAQ @ CMAS]: http://www.cmaq-model.org/
[GEIA_driver.sh]: ../../src/HEAD/GEIA_driver.sh?at=master
[reformat_GEIA_to_netCDF.r]: ../../src/HEAD/reformat_GEIA_to_netCDF.r?at=master
[regrid_global_to_AQMEII.r]: ../../src/HEAD/regrid_global_to_AQMEII.r?at=master
[retemp_reunit.ncl]: ../../src/HEAD/retemp_reunit.ncl?at=master
[IOAPI]: http://www.baronams.com/products/ioapi/
[VERDI]: http://www.verdi-tool.org/
[check_conservation.ncl]: ../../src/HEAD/check_conservation.ncl?at=master
[GEIA_N2O_oceanic_regrid.pdf]: ../../downloads/GEIA_N2O_oceanic_regrid.pdf

# TODOs

1. Retest with newest [`regrid_utils`][regrid_utils]! Currently, [`repo_diff.sh`][regrid_utils/repo_diff.sh] shows the following local `diff`s:
    * `string.ncl`
    * `summarize.ncl`
1. Move all these TODOs to [issue tracker][GEIA_regrid issues].
1. Create common project for `regrid_resources` à la [regrid_utils][], so I don't hafta hunt down which resource is in which project.
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. (this project only?) Use the EPIC input as template.
1. All regrids: how to nudge off/onshore as required? e.g., soil or burning emissions should never be offshore, marine emissions should never be onshore.
1. All regrid maps: add Caribbean islands (esp Bahamas! for offshore burning), Canadian provinces, Mexican states.
1. Complain to ncl-talk about NCL "unsupported extensions," e.g., `.ncf` and `<null/>` (e.g., MCIP output).
1. Determine why `<-` assignment is occasionally required in calls to `visualize.*(...)`.
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)

[GEIA_regrid issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master
